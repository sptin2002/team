package com.cpt202.team.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.ui.Model;;

@Controller
public class MainController {

    // http://localhost:8080/
    @GetMapping("/")
    public String home(Model model){
        model.addAttribute("username", "Thomas");
        return "home";
    }
}
